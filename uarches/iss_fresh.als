/* BOOM issue stage implementation. Used the ThreeStage.als file from the
 * CheckMate tutorial as a guideline
 */
module issue
open boom

one sig Iss extends Location { }
one sig Next extends Location { }
one sig Comp extends Location { }

// Define the operation path
fact min_nodes {
    Uop->{ Iss + Next + Comp} in NodeRel
}

fact max_nodes {
    NodeRel in Uop->{ Iss + Next + Comp}
}

fun get_op_path : Location->Location {
    Iss->Next + Next->Comp
}

fact op_path { all u : Uop | all disj l, l' : Location |
    l->l' in get_op_path <=> EdgeExists[u, l, u, l', uhb_intra]
}

// Constrain legal movement allong the path
fact bound_uhb_inter { uhb_inter in
	Event->Iss->Event->Iss
	+ Event->Next->Event->Next
    + Event->Comp->Event->Comp
    + Event->Iss->Event->Next
    + Event->Next->Event->Iss
    + Event->Iss->Event->Comp
}

fact iss_next_edge_prev_only { all u0, u1 : Uop |
    ProgramOrder[u0, u1] <=> (EdgeExists[u0, Next, u1, Iss, uhb_inter] or EdgeExists[u1, Iss, u0, Next, uhb_inter])
}

fact iss_inorder { all disj u0, u1 : Uop |
    ProgramOrder[u0, u1] <=> EdgeExists[u0, Iss, u1, Iss, uhb_inter]
}

fact next_ooo_data_dep { all disj u0, u1 : Uop |
    // Data_Dep[u0, u1] <=> EdgeExists[u0, Comp, u1, Next, uhb_inter]
    EdgeExists[u0, Iss, u1, Iss, uhb_inter] <=> EdgeExists[u0, Next, u1, Next, uhb_inter]
}

fact complete_inorder { all disj u0, u1 : Uop |
    EdgeExists[u0, Next, u1, Next, uhb_inter] <=> EdgeExists[u0, Comp, u1, Comp, uhb_inter]
}

pred Iss_Cap[cap :Int] { all u1 : Uop | #{u0 : Uop |
    ProgramOrder[u0, u1] and EdgeExists[u1, Iss, u0, Next, uhb_inter]} <= cap
}

pred Data_Dep[u0, u1 : Uop] {
    ProgramOrder[u0, u1] and (u0.RD = u1.RS1 or u0.RD = u1.RS2)
}

// Facts that would be enforced by other stages
pred reg_assign {
	all disj u0, u1 : Uop | EdgeExists[u1, Iss, u0, Comp, uhb_inter] => u1.RD != u0.RD
    all u : Uop | (
	    (u.RS1 in {ZeroReg + (PrecedingOps[u]).RD - u.RD})
	    and (u.RS2 in {ZeroReg + (PrecedingOps[u]).RD - u.RD})
	)
}


// Run the test
run test_issue {
    ucheck
    and Iss_Cap[2]
    and Prune_CheckMate
    // and reg_assign

    all u : Uop | u.process in Proc // assign all uops to the dummy process
	all disj u, u' : Uop | SameThread[u, u']

} for 12 but 1 Core, 3 Process, exactly 3 Event, exactly 3 Uop, 3 Location, 1 PhyRegIdx, 0 CacheIndexL1, 0 Address, 7 Int
