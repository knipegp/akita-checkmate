/* BOOM issue stage implementation. Used the ThreeStage.als file from the */
/* CheckMate tutorial as a guideline */
module issue
open boom

one sig Proc extends Process { }

/* Locations */
one sig Iss extends Location { }
one sig Next extends Location { }
one sig Complete extends Location {}

/* All Nodes */
fact bound_nodes_min {
    Uop->{ Iss + Next + Complete } in NodeRel
}

fact bound_nodes_max {
    NodeRel in Uop->{ Iss + Next + Complete }
}

/* Define paths */
fun op_path : Location->Location {
    Iss->Next +
	Next->Complete
}

/* Draw the edges */
fact Uop_completed { all u : Uop | all disj l, l' : Location |
    l->l' in op_path <=> EdgeExists[u, l, u, l', uhb_intra]
}

/*
 * Describe location order
 */
fact iss_add_inorder { all disj u0, u1 : Uop |
    ProgramOrder[u0, u1] <=>
	EdgeExists[u0, Iss, u1, Iss, uhb_inter]
}

fact arrive_next_inorder { all disj u0, u1 : Uop |
    EdgeExists[u0, Iss, u1, Iss, uhb_inter]
    <=> EdgeExists[u0, Iss, u1, Iss, uhb_inter]
}
// fact arrive_next_ooo { all disj u0, u1 : Uop |
//     EdgeExists[u0, Complete, u1, Next, uhb_inter]
//     => EdgeExists[u0, Next, u1, Next, uhb_inter]
// }

fact issue_complete_inorder { all disj u, u' : Uop |
    EdgeExists[u, Next, u', Next, uhb_inter]
    <=> EdgeExists[u, Complete, u', Complete, uhb_inter]
}

// fact enforce_data_dep { all disj u0, u1 : Uop |
//     (ProgramOrder[u0, u1] and (u0.RD = u1.RS1 or u0.RD = u1.RS2))
// 	<=> EdgeExists[u0, Complete, u1, Next, uhb_inter]
// }

/*
 * Constrain legal operations
 */
// pred Iss_Cap[cap :Int] { all u1 : Uop | #{u0 : Uop |
//     ProgramOrder[u0, u1] and EdgeExists[u1, Iss, u0, Next, uhb_inter]} <= cap
// }

run test_issue {
    ucheck
    and

    all disj u0, u1 : Uop | ProgramOrder[u0, u1]
} for 12 but 1 Core, 3 Process, 4 Event, 3 Location, 0 PhyRegIdx, 0 MemoryEvent, 0 CacheIndexL1, 0 Fence, 0 Address
// run test_issue {
//     ucheck
//     and Iss_Cap[2]
//     and #Uop = 4
//
//     all disj u0, u1 : Uop | ProgramOrder[u0, u1]
// } for 12 but 1 Core, 1 Process, 4 Event, 3 Location, 8 PhyRegIdx, 0 MemoryEvent, 0 CacheIndexL1, 0 Fence
// 12 for 4 events * 3 locations
