module boom
open checkmate/uarches/checkmate_tutorial

one sig Proc extends Process { }
sig CacheLine { }

// sig BrTag { }
sig PhyRegIdx { }

one sig ZeroReg extends PhyRegIdx { }

sig Uop extends Event {
// PC: one PhysicalAddress,
    InsnCacheLine: one CacheLine,
    RD: lone PhyRegIdx,
    RS1: lone PhyRegIdx,
    RS2: lone PhyRegIdx,
    // BrMask: set BrTag
}

fun PrecedingOps[u1 : Uop] : Uop {
    {u0 : Uop | ProgramOrder[u0, u1]}
}

// Ensure that relations from other CheckMate models aren't added to issue
// stage's solutions.
pred Prune_CheckMate {
	#urf=0
	#uco=0
	#ufr=0
	#ustb_flush=0
	#ustb=0
	#uvicl=0
	#ucci=0
	#ufence=0
	#uhb_proc=0
}

pred SameCacheLine[cl: CacheLine, cl': CacheLine] { cl->cl' in iden }
