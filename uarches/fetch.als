// Header
module fetch
open boom

// Declare Locations
one sig Fetch extends Location { }
one sig DummyMem extends Location { }
one sig WriteICache extends Location { }
one sig ReadICache extends Location { }
one sig DummyExecute extends Location { }

// Constrain possible nodes
fact min_nodes {
    Uop->{Fetch + ReadICache + DummyExecute} in NodeRel
}

fact max_nodes { NodeRel in
    Uop->{Fetch + DummyMem + WriteICache + ReadICache + DummyExecute}
}


// Define possible paths for each type of event
fun LineExists : Event { Event - NodeRel.DummyMem }
fun LineDNE : Event { NodeRel.DummyMem }

fun UopInCache : Location->Location {
    Fetch->ReadICache +
	ReadICache->DummyExecute
}

fun UopInMem : Location->Location {
    Fetch->DummyMem +
    DummyMem->WriteICache +
    WriteICache->ReadICache +
	ReadICache->DummyExecute
}

fact uop_path_in_cache {
    all u : Uop <: LineExists |
	all disj l0, l1 : Location |
    l0->l1 in UopInCache <=> EdgeExists[u, l0, u, l1, uhb_intra]
}

fact uop_path_in_mem {
    all u : Uop <: LineDNE |
	all disj l0, l1 : Location |
    l0->l1 in UopInMem <=> EdgeExists[u, l0, u, l1, uhb_intra]
}

// Inter-instruction uhb relationships
fact fetch_in_order { all disj u0, u1 : Uop |
    ProgramOrder[u0, u1] <=>
	EdgeExists[u0, Fetch, u1, Fetch, uhb_inter]
}

fact mem_in_order { all disj u0, u1 : Uop |
    (ProgramOrder[u0, u1] and NodeExists[u0, DummyMem] and NodeExists[u1, DummyMem])<=>
	EdgeExists[u0, DummyMem, u1, DummyMem, uhb_inter]
}

fact write_in_order { all disj u0, u1 : Uop |
    (ProgramOrder[u0, u1] and NodeExists[u0, WriteICache] and NodeExists[u1, WriteICache])<=>
	EdgeExists[u0, WriteICache, u1, WriteICache, uhb_inter]
}

fact exec_in_order { all disj u0, u1 : Uop |
    ProgramOrder[u0, u1] <=>
	EdgeExists[u0, DummyExecute, u1, DummyExecute, uhb_inter]
}

fact read_in_order { all disj u0, u1 : Uop |
    ProgramOrder[u0, u1] <=>
	EdgeExists[u0, ReadICache, u1, ReadICache, uhb_inter]
}

// Other constraints on event ordering
fact line_loaded {
    all u1 : Uop <: LineExists | some u0 : Uop | ProgramOrder[u0, u1] and SameCacheLine[u0.InsnCacheLine, u1.InsnCacheLine]
}

// Define the test
run test_fetch {
    ucheck
    and Prune_CheckMate

    all u : Uop | u.process in Proc // assign all uops to the dummy process
	all disj u0, u1 : Uop | SameThread[u0, u1]

} for 16 but 1 Core, 3 Process, exactly 3 Event, exactly 3 Uop, 5 Location, 1 PhyRegIdx, 0 CacheIndexL1, 0 Address, 2 CacheLine
