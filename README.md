# Akita-CheckMate

We are integrating [CheckMate](https://github.com/ctrippel/checkmate) into
[Akita](gitlab.com/akita/akita) simulations to facilitate security verification
in unison with microarchitecture performance evaluation. This project aims to
automatically generate Alloy DSL from Go simulation models. Current work focuses 
on manually translating models to Alloy. The translation will be used to inform
the software architecture of models.

## Building CheckMate

Users can build CheckMate by following the README instructions at the CheckMate
repository or through our [Docker
container](https://hub.docker.com/repository/docker/knipegp/checkmate).

Users will likely want to access CheckMate generated files outside of the
container file system. We recommend using this
[script](https://github.com/knipegp/docker-base) which creates a container user
with the same UID and GID as the host user and attaches the specified host
directory to the container. This way, CheckMate will generate files on the host
file system with the correct permissions.
