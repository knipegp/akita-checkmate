#!/usr/bin/env bash

java -cp /opt/checkmate/alloy-analyzer/dist/alloy4.2.jar edu.mit.csail.sdg.alloy4whole.MainClass "$@"
