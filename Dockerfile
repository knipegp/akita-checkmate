FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && apt-get install -y \
    git \
    openjdk-8-jdk \
    graphviz \
    python \
    curl \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /opt
RUN curl -LO https://mirrors.ocf.berkeley.edu/apache//ant/binaries/apache-ant-1.9.15-bin.tar.gz \
    && tar -zxf apache-ant-1.9.15-bin.tar.gz \
    && rm -rf apache-ant-1.9.15-bin.tar.gz
ENV PATH="$PATH":/opt/apache-ant-1.9.15/bin
RUN git clone https://github.com/ctrippel/checkmate.git

WORKDIR /opt/checkmate
RUN git clone https://gitlab.com/knipegp/alloy-analyzer.git

WORKDIR /opt/checkmate/alloy-analyzer
RUN ant dist
RUN cp ../MainClass.java edu/mit/csail/sdg/alloy4whole/ && ant dist

WORKDIR /opt/checkmate
COPY ./checkmate.sh /opt/checkmate/checkmate.sh
RUN chmod +x ./checkmate.sh && ln -s /opt/checkmate/checkmate.sh /usr/bin/checkmate
RUN ln -s /opt/checkmate/util/release-generate-graphs.py /usr/bin/release-generate-graphs \
    && ln -s /opt/checkmate/util/release-generate-images.py /usr/bin/release-generate-images \
    && ln -s /opt/checkmate/util/release-symmetry-reduction.py /usr/bin/release-symmetry-reduction

WORKDIR /root
